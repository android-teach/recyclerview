package ictu.edu.vn.android.recyclerview;

public class Deal {
    private int id;
    private String name;
    private String description;
    private double price;

    /* groupId is id of group for deal, eg: food, family, friend, shopping
     * it also id or image to display for item
     */
    private int groupId;

    public Deal(int id, String name, String description, double price, int groupId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.groupId = groupId;
    }

    public int getId() {
        return id;
    }

    public Deal setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Deal setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Deal setDescription(String description) {
        this.description = description;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Deal setPrice(double price) {
        this.price = price;
        return this;
    }

    public int getGroupId() {
        return groupId;
    }

    public Deal setGroupId(int groupId) {
        this.groupId = groupId;
        return this;
    }
}

