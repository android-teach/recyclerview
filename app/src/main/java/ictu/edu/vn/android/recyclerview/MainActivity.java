package ictu.edu.vn.android.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Deal> dealList = new ArrayList<>();
    private DealAdapter adapter;
    private RecyclerView rvDeal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindView();
        loadData();
    }

    private void bindView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        adapter = new DealAdapter(this, dealList);
        rvDeal = findViewById(R.id.rvDeal);
        rvDeal.setLayoutManager(layoutManager);
        rvDeal.setAdapter(adapter);
    }

    private void loadData() {
        // load your data here, in this, I create some example of data

        for (int i = 1; i <= 20; i++) {
            Deal deal = new Deal(i, "Giao dịch thứ " + i, "mô tả thứ " + i, i * 1000, R.drawable.ic_restaurant);
            dealList.add(deal);
        }

        // change data to display on view
        adapter.notifyDataSetChanged();
    }
}
